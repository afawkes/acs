﻿New-AzureRmResourceGroup -Name "acs-test" -Location "UK South" -Force

New-AzureRmResourceGroupDeployment -Name "ACSTest" -ResourceGroupName "acs-test" -TemplateFile "C:\Users\Anthony.Fawkes\Documents\acs\azuredeploy.json" -TemplateParameterFile "C:\Users\Anthony.Fawkes\Documents\acs\azuredeploy.parameters.json"